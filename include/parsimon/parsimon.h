#ifndef PARSIMON_INCLUDE_H
#define PARSIMON_INCLUDE_H

#include "parsimon/lazy.h"
#include "parsimon/monad.h"
#include "parsimon/result.h"
#include "parsimon/state.h"
#include "parsimon/settings.h"
#include "parsimon/core.h"
#include "parsimon/combinators.h"
#include "parsimon/parsers.h"

#endif // TOKEN_PARSIMON_H
